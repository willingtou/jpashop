package jpabook.jpashop.domain.Item;

import jpabook.jpashop.domain.Category;
import jpabook.jpashop.exception.NotEnoughStockException;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter @Setter
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "dtype")
public abstract class Item {

    @Id @GeneratedValue
    @Column(name = "item_id")
    private Long id;

    private String name;
    private int price;
    private int stockQuantity;


    /*
    객체는 컬렉션이있어서 다대다 관계가 가능하지만
    관계형 db는 컬렉션관계를 양쪽에 가질 수 있는것이 아니기떄문에
    중간테이블이 있어야함 ( JoinTable )
     */
    @ManyToMany(mappedBy = "items")
    private List<Category> categories = new ArrayList<>();

    // == 비즈니스 로직== //
    // 객체지향적이게 응집도를 높이기위해 stockQuantity가 있는 이 클래스에 로직을 추가함.
    /**
     * 즉, 엔티티에 직접적으로 핵심비즈니스로직을 넣는것이 좋음.
     * setter 대신에 비즈니스별도 메서드가 여기에 해당.
     * 재고 (stock) 증가 , 감소
    */
    public void addStock(int quantity){
        this.stockQuantity+=quantity;
    }
    public void removeStock(int quantity){
        int restStock = this.stockQuantity-quantity;
        if(restStock<0){
            throw new NotEnoughStockException("need more stock");
        }
        this.stockQuantity=restStock;
    }
    public void change(String name,int price,int stockQuantity)
    {
        this.name=name;
        this.price=price;
        this.stockQuantity=stockQuantity;
    }
}
