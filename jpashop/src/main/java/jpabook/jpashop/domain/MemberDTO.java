package jpabook.jpashop.domain;

import lombok.Data;

@Data
public class MemberDTO {

    private String name;
    private Integer age;
}
