package jpabook.jpashop.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter @Setter
public class Delivery {

    @Id @GeneratedValue
    @Column(name = "delivery_id")
    private Long id;

    @OneToOne(mappedBy = "delivery" ,fetch = FetchType.LAZY)
    private Order order;

    @Embedded
    private Address address;


    /*
    enum타입은 @Enumerated애노태이션을 넣어야하고 ,
    enum타입을 넣을떄 ordinal 이랑 String중 ordinal이 default임.
    중간에 다른게생기면 밀리기떄문에 ordinal이 아닌 string 을 써야함.
     */
    @Enumerated(EnumType.STRING)
    private DeliveryStatus status; //READY , COMP
}
