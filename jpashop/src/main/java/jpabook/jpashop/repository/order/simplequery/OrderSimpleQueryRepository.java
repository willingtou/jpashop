package jpabook.jpashop.repository.order.simplequery;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

//queryRepository로 별도로 빼는것이 좋음.
@Repository
@RequiredArgsConstructor
public class OrderSimpleQueryRepository {

    private final EntityManager em;

    //V4
    // 엔티티(Order)을 넘기면 식별자로 넘어오기때문에 파라미터에 엔티티x
    public List<OrderSimpleQueryDto> findOrderDtos() {
        return em.createQuery("select new jpabook.jpashop.repository.order.simplequery.OrderSimpleQueryDto(o.id,m.name,o.orderDate,o.status,d.address) " +
                " from Order o" +
                " join o.member m"+
                " join o.delivery d",OrderSimpleQueryDto.class
        ).getResultList();
    }
}
