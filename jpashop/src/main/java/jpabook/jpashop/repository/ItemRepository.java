package jpabook.jpashop.repository;

import jpabook.jpashop.domain.Item.Item;

import java.util.List;

public interface ItemRepository {

    public void save(Item item);
    public Item findOne(Long itemId);
    public List<Item> findAll();
}
