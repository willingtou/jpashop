package jpabook.jpashop.repository;

import jpabook.jpashop.domain.Item.Item;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class JpaItemRepository implements ItemRepository{

    private final EntityManager em;

    @Override
    public void save(Item item) {

        if (item.getId() == null)
            em.persist(item);
        else
            em.merge(item);
    }

    @Override
    public Item findOne(Long itemId) {
        return em.find(Item.class,itemId);
    }
    @Override
    public List<Item> findAll() {
        return em.createQuery("select i from Item i",Item.class)
                .getResultList();
    }
}
