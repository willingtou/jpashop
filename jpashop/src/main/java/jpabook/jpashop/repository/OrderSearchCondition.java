package jpabook.jpashop.repository;

import com.querydsl.core.types.dsl.BooleanExpression;
import jpabook.jpashop.domain.OrderStatus;

public interface OrderSearchCondition {

    public BooleanExpression statusEq(OrderStatus statusCond);
    public BooleanExpression nameLike(String nameCond);
}
