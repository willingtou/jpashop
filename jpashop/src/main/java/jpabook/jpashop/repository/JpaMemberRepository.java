package jpabook.jpashop.repository;

import jpabook.jpashop.domain.Member;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class JpaMemberRepository implements MemberRepository{

    /*
        @PersistenceContext // 스프링이 EntityManager를 자동으로 주입시켜줌
        private EntityManager em;
        원래는 @Autowired로 em을 필드주입이 안되지만
        스프링부트에서 지원을해줌 -> @PersistenceContext 대신 사용가능.
        그러나 일관성있게 @RequiredArgsConstructor 를 사용하였음.
     */

    private final EntityManager em;

    @Override
    public void save(Member member) {
        em.persist(member);
    }

    @Override
    public Member findOne(Long id) {
        return em.find(Member.class,id);
    }

    @Override
    public List<Member> findAll() {
        return em.createQuery("select m from Member m ",Member.class)
                .getResultList();
    }

    @Override
    public List<Member> findByName(String name) {
        return em.createQuery("select m from Member m where m.name= : name",Member.class)
                .setParameter("name",name).getResultList();
    }
}
