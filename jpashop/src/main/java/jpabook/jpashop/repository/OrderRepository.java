package jpabook.jpashop.repository;

import jpabook.jpashop.domain.Order;
import jpabook.jpashop.repository.order.simplequery.OrderSimpleQueryDto;

import java.util.List;

public interface OrderRepository {

    public void save(Order order);
    public Order findOne(Long id);
    public List<Order> findAll(OrderSearch orderSearch);
    public List<Order> findAllByString(OrderSearch orderSearch);
    public List<Order> findAllByCriteria(OrderSearch orderSearch);
    public List<Order> findAllWithMemberDelivery();
    public List<Order> findAllWithItem();

    public List<Order> findAllWithMemberDelivery(int offset, int limit);
}
