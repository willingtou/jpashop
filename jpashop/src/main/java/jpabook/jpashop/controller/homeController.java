package jpabook.jpashop.controller;

import jpabook.jpashop.domain.MemberDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Slf4j
public class homeController {

    @GetMapping("/")
    public String home()
    {
        log.info("home controller");
        return "home";
    }

    @ResponseBody
    @GetMapping("/json")
    public MemberDTO test()
    {
        MemberDTO member = new MemberDTO();
        member.setAge(20);
        member.setName("kim");
        return member;
    }
}
