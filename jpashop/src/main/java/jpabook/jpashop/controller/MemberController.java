package jpabook.jpashop.controller;

import jpabook.jpashop.domain.Address;
import jpabook.jpashop.domain.Member;
import jpabook.jpashop.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/members")
@RequiredArgsConstructor
public class MemberController {

    private final MemberService memberService;

    @GetMapping("/new")
    public String createForm(Model model){
        model.addAttribute("memberForm",new MemberForm());
        return "members/createMemberForm";
    }

    /**
     javaX의 표준 validation을 쓰는것을 인식하는 @valid 애노테이션으로
     MemberForm 클래스의 @NotEmpty를 애노테이션을읽음.
     그리고 BindingResult에서 @Valid를 인식.

     ->
     createMemberForm.html에서 fields.hasErrors 처리해논것중
     미리 정의해둔것으로 빨간색테투리와 , 미리정해둔 프로퍼티에대한 에러를 렌더링.
     */
    @PostMapping("/new")
    public String create(@Valid MemberForm form, BindingResult result)
    {
        if(result.hasErrors())
            return "members/createMemberForm";

        Address address = new Address(form.getCity(), form.getStreet(), form.getZipcode());
        Member member = new Member();
        member.setName(form.getName());
        member.setAddress(address);

        memberService.join(member);
        return "redirect:/";
    }

    @GetMapping
    public String list(Model model)
    {
        List<Member> members = memberService.findMembers();
        model.addAttribute("members",members);
        return "members/memberList";
    }
}
