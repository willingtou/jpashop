package jpabook.jpashop.service;

import jpabook.jpashop.domain.Member;
import jpabook.jpashop.repository.JpaMemberRepository;
import jpabook.jpashop.repository.MemberRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class MemberServiceTest {    

    @Autowired MemberService memberService;
    @Autowired MemberRepository memberRepository;
    /*
        //@Autowired EntityManager em; Rollback 를 하지않게 하거나
        em을 주입받게해서 em.flush()를 발생시키면 insert문이 생성됨.
     */


    /*
        //@Rollback(false) -> insert문이 commit시점에 발생하게함.
        by 롤백을 하게 하지 않음으로서.
     */
    @Test
    public void 회원가입() throws Exception
    {
        //given
        Member member = new Member();
        member.setName("kim");

        //when
        Long savedId = memberService.join(member);

        //then
        assertEquals(member,memberRepository.findOne(savedId));

    }
    
    @Test(expected = IllegalStateException.class)
    public void 중복_회원_예외() throws Exception
    {
        //given
        Member member1 = new Member();
        Member member2 = new Member();
        member1.setName("kim");
        member2.setName("kim");

        //when
        memberService.join(member1);
        memberService.join(member2);


        //then
        Assert.fail("예외가 발생해야 한다.");
    }


}